const gulp = require("gulp");
const concat = require("gulp-concat");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const del = require("del");
const browserSync = require("browser-sync").create();
const size = require("gulp-size");
const replace = require("gulp-replace");

const sass = require("gulp-sass")(require('sass'));
const sourcemaps = require("gulp-sourcemaps");
const rev = require("gulp-rev");


const nunjucksRender = require("gulp-nunjucks-render");
const data = require("gulp-data");
const fs = require("fs");

const imagemin = require("gulp-imagemin");
const pngquant = require("imagemin-pngquant");
const plumber = require("gulp-plumber");

const svgSprite = require("gulp-svg-sprites");
const svgSymbols = require('gulp-svg-symbols')
const svgmin = require("gulp-svgmin");
const svg2string = require("gulp-svg2string");
const stripBom = require("gulp-stripbom");

const uglify = require("gulp-uglify");

sass.compiler = require("node-sass");

const cssFiles = [
    // "./node_modules/normalize.css/normalize.css",
    "./src/css/**/*.css"
];

const scssFiles = [
    // "./node_modules/normalize.css/normalize.css",
    // "./node_modules/animate.css/animate.css",
    "./src/scss/main.scss"
];

const jsFiles = [
    "./src/js/jquery.min.js",
    "./src/js/plugins/**/*.js",
    "./src/js/*.js"
];

const nunjucksFiles = ["./src/pages/**/*.njk", "./src/templates/**/*.njk"];
const imgSrc = "./src/images/**/*";
const imgSvg = "./src/images/svg-icons/*.svg";

const staticFiles = ["./src/scss/font/**/*"];

function styles() {
    return gulp
        .src(cssFiles)
        .pipe(concat("style.css"))
        .pipe(
            autoprefixer({
                browsers: [">0.1%"],
                cascade: false
            })
        )
        .pipe(
            cleanCSS({
                level: 2
            })
        )
        .pipe(gulp.dest("./build/css"))
        .pipe(browserSync.stream());
}

function sass_style(mode) {
    if (mode == "prod") {
        return (
            gulp
                .src(scssFiles)
                .pipe(concat("style.css"))
                .pipe(sourcemaps.init())
                .pipe(sass().on("error", sass.logError))
                .pipe(autoprefixer("last 1 version", "> 1%", "ie 8", "ie 7"))
                .pipe(
                    cleanCSS({
                        level: 2
                    })
                )
                .pipe(sourcemaps.write("./"))
                //.pipe(rev())
                .pipe(gulp.dest("./build/css"))
            /*.pipe(rev.manifest('./build/rev-manifest.json', {
                  merge: true
              }))
              .pipe(gulp.dest('./'))*/
        );
    } else {
        return gulp
            .src(scssFiles)
            .pipe(concat("style.css"))
            .pipe(sourcemaps.init())
            .pipe(sourcemaps.identityMap())
            .pipe(sass().on("error", sass.logError))
            .pipe(sourcemaps.write("./"))
            .pipe(gulp.dest("./build/css"))
            .pipe(browserSync.stream());
    }
}

function script(mode) {
    if (mode == "prod") {
        return gulp
            .src(jsFiles)
            .pipe(concat("uglified.js"))
            .pipe(
                size({
                    title: "source js"
                })
            )
            .pipe(
                uglify({
                    toplevel: true
                })
            )
            .pipe(
                size({
                    title: "uglified js"
                })
            )
            .pipe(gulp.dest("./build/js"));
    } else {
        let js_files = jsFiles;
        js_files.push("./src/js/not-uglified.js");
        return gulp
            .src(js_files)
            .pipe(concat("main.js"))
            .pipe(gulp.dest("./build/js"))
            .pipe(browserSync.stream());
    }
}

function not_uglified_script(mode) {
    if (mode == "prod") {
        gulp
            .src([
                "./build/js/uglified.js",
                "./src/js/not-uglified.js"
            ])
            .pipe(concat("main.js"))
            .pipe(
                size({
                    title: "source js"
                })
            )
            .pipe(
                uglify({
                    toplevel: true,
                    keep_fnames: true,
                    compress: {
                        drop_console: true,
                        unused: false
                    }
                })
            )
            .pipe(
                size({
                    title: "minified js"
                })
            )
            .pipe(gulp.dest("./build/js"))
            .pipe(browserSync.stream());
        return del(["build/js/uglified.js"]);
    }
}

function clear() {
    return del(["build/*"]);
}

function moveStaticFiles() {
    // return gulp.src(staticFiles, { base: 'src' })
    //     .pipe(gulp.dest('build'))
    return gulp
        .src("./src/scss/font/**/*", {
            base: "src/scss"
        })
        .pipe(gulp.dest("build/css"));
}

function initPageList(cb) {
    let pages = fs.readdirSync("./src/pages/")
        .filter(f => {
            if (f === '.' || f === '..' || f[0] === '.') {
                return false;
            }
            return true;
        })
        .map(f => {
            let stat = fs.statSync("./src/pages/" + f);

            return {
                name: f.substring(0, f.lastIndexOf(".")) + ".html",
                birthtime:
                    stat.birthtime.toLocaleDateString() +
                    " " +
                    stat.birthtime.toLocaleTimeString(),
                mtime:
                    stat.mtime.toLocaleDateString() + " " + stat.mtime.toLocaleTimeString()
            };
        });
    let myJSON = JSON.parse(fs.readFileSync("./src/data.json"));
    myJSON.pages = pages;
    if (typeof myJSON.version == "undefined") {
        myJSON.version = 0;
    }
    myJSON.version += 1;
    fs.writeFile("./src/data.json", JSON.stringify(myJSON), function (err) {
        if (err) {
            return console.log(err);
        }
    });
    cb();
}

var bem_classes = {};
var manageEnvironment = function (environment) {
    environment.addFilter('bem', function (block_name, element_name, reset = false) {
        if (typeof bem_classes[block_name] == "undefined") {
            bem_classes[block_name] = {classes: [], lastIndex: -1};
        }
        if (reset) {
            bem_classes[block_name].lastIndex = -1;
        }

        ++bem_classes[block_name].lastIndex;
        let index = bem_classes[block_name].lastIndex;
        bem_classes[block_name].classes[index] = element_name;

        let class_name = block_name;
        console.log(class_name, element_name, index);
        for (let i = 0; i <= bem_classes[block_name].lastIndex; i++) {
            class_name = class_name + "__" + bem_classes[block_name].classes[i];
            console.log(i, bem_classes[block_name].classes[i], class_name);
        }

        return class_name;
    });
}

function nunjucks() {
    return (
        gulp
            .src(nunjucksFiles)
            .pipe(plumber())
            .pipe(
                data(function () {
                    return JSON.parse(fs.readFileSync("./src/data.json"));
                })
            )
            // Renders template with nunjucks
            .pipe(
                nunjucksRender({
                    path: ["src/templates"],
                    manageEnv: manageEnvironment
                })
            )
            // output files in app folder
            .pipe(gulp.dest("build"))
            .pipe(browserSync.stream())
    );
}

function images() {
    return gulp
        .src(imgSrc)
        .pipe(plumber())
        .pipe(
            imagemin({
                progressive: true,
                svgoPlugins: [
                    {
                        removeViewBox: false
                    }
                ],
                use: [pngquant()]
            })
        )
        .pipe(gulp.dest("./build/images"));
}

function sprites() {
    return (
        gulp
            .src(imgSvg)
            //.pipe(stripBom())
            .pipe(
                svgmin({
                    plugins: [{inlineStyles: {onlyMatchedOnce: false}}]
                })
            )
            .pipe(
                svgSymbols({templates: [`default-svg`], id: "svg-%f"})
            )
            .pipe(replace(/<svg(.*?)(width="[0-9.]*" height="[0-9.]*")/g, "<svg$1"))
            .pipe(svg2string())
            .pipe(gulp.dest("./build/js"))
            .pipe(browserSync.stream())
    );
}

function watch() {
    browserSync.init({
        server: "./build/"
    });

    //gulp.watch('./src/css/**/*.css', styles);
    gulp.watch("./src/scss/**/*.scss", sass_style);
    gulp.watch("./src/js/**/*.js", script);
    gulp.watch(nunjucksFiles, gulp.series(initPageList, nunjucks));
    gulp.watch(imgSrc, images);
    gulp.watch(imgSvg, sprites);
    //gulp.watch("./*.html").on('change', browserSync.reload);
}

gulp.task("sprites", sprites);
gulp.task("imgmin", gulp.series(images, sprites));
gulp.task("sass_style", sass_style.bind(sass_style, "prod"));
gulp.task("script", script);
gulp.task("not_uglified_script", not_uglified_script);
gulp.task("moveStaticFiles", moveStaticFiles);
gulp.task(
    "build",
    gulp.series(
        clear,
        initPageList,
        gulp.parallel(
            moveStaticFiles,
            nunjucks,
            sass_style.bind(sass_style, "prod"),
            script.bind(script, "prod"),
            "imgmin"
        ),
        not_uglified_script.bind(not_uglified_script, "prod")
    )
);
gulp.task("nunjucks", nunjucks);
gulp.task("initPageList", initPageList);
gulp.task("watch", watch);
